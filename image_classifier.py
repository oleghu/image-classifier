import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.keras import datasets, layers, models

class_names = ['Plane', 'Car', 'Bird', 'Cat', 'Deer', 'Dog', 'Frog', 'Horse', 'Ship', 'Truck']

model = models.load_model('image_classifier.model')

img = cv.imread('frog.jpg')
img = cv.cvtColor(img, cv.COLOR_BGR2RGB)

plt.imshow(img, cmap=plt.cm.binary)
plt.show()

prediciton = model.predict(np.array([img]) / 255)
index = np.argmax(prediciton)
print(f'prediciton is {class_names[index]}')