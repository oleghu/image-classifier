## Image Classifier, Neural Network

This is a neural network that classifies images of 10 different things:

'Plane', 'Car', 'Bird', 'Cat', 'Deer', 'Dog', 'Frog', 'Horse', 'Ship', 'Truck'

To use the program, download the image_classifier_model_builder file. Be sure to have the following libraries installed:

 - numpy
 - matplotlib
 - tensorflow
 - opencv-python

 Run the modelbuilder. You can adjust the amount of data you would like to train your model with by adjusting line 20-23.

 When the model is finished you can download image_classifier file and use this to test your model on any picture you want. Just be sure to edit it to a 32x32 pixel format.

 You could alo just download the already built model and us it with the image_classifier file.
